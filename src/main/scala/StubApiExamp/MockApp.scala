package StubApiExamp

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import org.slf4j.LoggerFactory
import org.slf4j.Logger


object MockApp {

  val logger: Logger = LoggerFactory.getLogger(getClass)


  val getBody: String =
    s""" {
       |"State": "Full",
       |"Citizenship": "Rus"
       |} """.stripMargin


  val errorBody: String =
    s""" {
       |"status": "Error",
       |"message": "Endpoint not found",
       |} """.stripMargin


  def main(args: Array[String]): Unit = {
    val wireMockServer = new WireMockServer(wireMockConfig.port(8089))
    wireMockServer.start()

    wireMockServer.stubFor(

      WireMock
        .get(urlMatching("/api/*"))
        .withHeader("Accept", equalTo("application/json, text/plain, */*"))
        .withHeader("Connection", equalTo("keep-alive"))
        .withHeader("User-Agent", equalTo("Mozilla/5.0 " +
            "(Windows NT 10.0; Win64; 64) " +
            "AppleWebkit/537.36 (KHTML, Like Gecko) " +
            "Chrome/109.0.0.0 Safari/537.36"))
        .withHeader("Content-Type", equalTo("application/json"))

        .willReturn(
          aResponse
            .withStatus(404)
            .withBody(errorBody)
        )

    )

    wireMockServer.stubFor(
      WireMock
        .get(urlEqualTo("/api/v1")).atPriority(1)
        .withHeader("Accept", equalTo("application/json, text/plain, */*"))
        .withHeader("Connection", equalTo("keep-alive"))
        .withHeader("User-Agent", equalTo("Mozilla/5.0 " +
          "(Windows NT 10.0; Win64; 64) " +
          "AppleWebkit/537.36 (KHTML, Like Gecko) " +
          "Chrome/109.0.0.0 Safari/537.36"))
        .withHeader("Content-Type", equalTo("application/json"))

        .willReturn(
          aResponse
            .withStatus(200)
            .withBody(getBody)
            .withHeader("Content-Type", "application/json;charset=utf-8")
        ))

    wireMockServer.stubFor(
      WireMock
        .get(urlMatching("/api/v1")).atPriority(5)
        .willReturn(aResponse
          .withStatus(404)
          .withBody(errorBody)
        )
    )

  }
}
