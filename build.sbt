name := "ExampMockServ"
version := "0.1.0-SNAPSHOT"
scalaVersion := "2.13.10"
organization := "StubApiExamp"
libraryDependencies ++= Seq(

  "com.github.tomakehurst" % "wiremock" % "2.27.2",
  "org.scalatest" %% "scalatest" % "3.0.8",
  "ch.qos.logback" % "logback-classic" % "1.2.10",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
)

