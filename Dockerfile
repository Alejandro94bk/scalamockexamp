FROM openjdk:8-jre

RUN apt-get update \
    && apt-get nano \
    && apt-get curl

RUN echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d \
    && apt-key adv --keyserver hpk://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 \
    && apt-get update \
    && apt-get install -y sbt

RUN mkdir /var/srv
WORKDIR /var/srv

COPY . /var/srv
RUN sbt compile

ENTRYPOINT ["sbt"]
CMD ["run"]
